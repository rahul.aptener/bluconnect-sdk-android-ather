package com.aptener.bluarmor.test.app.athersample

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.aptener.bluarmor.test.app.athersample.ui.theme.AtherSampleTheme
import com.aptener.bluconnect.common.ext.asMutableStateFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MainActivity : ComponentActivity(), ServiceConnection {
    private val bluService: StateFlow<BluService?> = MutableStateFlow(null)

    private val vm = MainViewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            AtherSampleTheme {
                ContentView(vm)

                /*  Scaffold(modifier = Modifier.fillMaxSize()) { innerPadding ->
                      ContentView(
                          name = "Android",
                          modifier = Modifier.padding(innerPadding)
                      )
                  }*/
            }
        }
    }

    override fun onStart() {

        with(Intent(this, BluService::class.java)) {
            startService(this)
        }

        // Bind to LocalService
        Intent(this, BluService::class.java).also { intent ->
            bindService(intent, this, Context.BIND_AUTO_CREATE)
        }

        super.onStart()

    }

    override fun onStop() {
        unbindService(this)
        super.onStop()
    }

    override fun onServiceConnected(className: ComponentName, service: IBinder) {
        val binder = service as BluService.BluServiceBinder
        bluService.asMutableStateFlow()?.tryEmit(binder.service)
    }

    override fun onServiceDisconnected(className: ComponentName) {
        bluService.asMutableStateFlow()?.tryEmit(null)
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    AtherSampleTheme {
        Greeting("Android")
    }
}