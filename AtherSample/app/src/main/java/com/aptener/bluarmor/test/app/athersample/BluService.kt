package com.aptener.bluarmor.test.app.athersample

import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.aptener.bluconnect.BluArmorModel
import com.aptener.bluconnect.BluConnectService
import com.aptener.bluconnect.client.ConnectionState
import com.aptener.bluconnect.device.AdvertisingBluArmorDevice
import com.aptener.bluconnect.device.BluArmorDevice
import com.aptener.bluconnect.device.HALO
import com.aptener.bluconnect.device.HALOBit
import com.aptener.bluconnect.device.HALOBitOta
import com.aptener.bluconnect.device.HALOOta
import com.aptener.bluconnect.device.control.FirmwareUpdate
import com.aptener.bluconnect.device.control.OtaBinaryPath
import com.aptener.bluconnect.device.control.OtaSwitch
import com.aptener.bluconnect.device.control.RideGrid
import kotlinx.coroutines.flow.MutableStateFlow

class BluService : BluConnectService() {

    override fun onCreate() {
        super.onCreate()
    }

    override fun onBluArmorDeviceDiscovered(result: Set<AdvertisingBluArmorDevice>) {

    }

    override fun onConnectionStateChange(state: ConnectionState) {
        BluService.onConnectionStateChangeListener?.invoke(state)
        when (state) {
            is ConnectionState.DeviceReady -> {
                BluService.device = state.device

                when {
                    device?.model == BluArmorModel.HALO -> {
                        setSideLoadOtaBinaries(OtaBinaryPath.OtaRawResource(R.raw.halo_ota))

                        otaProgress.value = 0
                        (device as? HALO)?.otaSwitch?.observe { otaState ->

                            BluService.otaState.value = when (otaState) {
                                OtaSwitch.OtaSwitchState.NoUpdateAvailable -> false
                                is OtaSwitch.OtaSwitchState.UpdateAvailable -> true
                            }

                            Log.e("OTA", "OTA: $otaState")
                        }

                        (device as? HALO)?.rideGrid?.musicSharing?.observe{ musicSharingState ->
                            musicSharing.value = musicSharingState
                            Log.e("MUSIC_SHARING", "MUSIC_SHARING: $musicSharingState")
                        }

                        (device as? HALO)?.rideGrid?.observe{ ridegridState ->
                            val chitChatShat = when (ridegridState){
                                RideGrid.RideGridState.OFF -> false
                                else -> true
                            }
                            chitChatState.value = chitChatShat
                            Log.e("RIDEGIRD_STATE", "RIDEGIRD_STATE: $ridegridState")
                        }


                    }

                    device?.model == BluArmorModel.HALO_OTA -> {
                        setSideLoadOtaBinaries(OtaBinaryPath.OtaRawResource(R.raw.halo_ota))

                        (device as? HALOOta)?.firmwareUpdate?.observe { otaState ->
                            Log.e("OTA", "OTA STAT: $otaState")
                            if (otaState is FirmwareUpdate.FirmwareUpdateState.Upgrading) {
                                Log.e("OTA", "OTA PRORESS: ${otaState.appProgress}")

                                BluService.otaProgress.value = otaState.appProgress
                            }

                        }
                    }


                    device?.model == BluArmorModel.HALO_BIT -> {
                        setSideLoadOtaBinaries(OtaBinaryPath.OtaRawResource(R.raw.halo_bit_ota))

                        otaProgress.value = 0
                        (device as? HALOBit)?.otaSwitch?.observe { otaState ->

                            BluService.otaState.value = when (otaState) {
                                OtaSwitch.OtaSwitchState.NoUpdateAvailable -> false
                                is OtaSwitch.OtaSwitchState.UpdateAvailable -> true
                            }

                            Log.e("OTA", "OTA: $otaState")
                        }
                    }

                    device?.model == BluArmorModel.HALO_BIT_OTA -> {
                        setSideLoadOtaBinaries(OtaBinaryPath.OtaRawResource(R.raw.halo_bit_ota))

                        (device as? HALOBitOta)?.firmwareUpdate?.observe { otaState ->
                            Log.e("OTA", "OTA STAT: $otaState")
                            if (otaState is FirmwareUpdate.FirmwareUpdateState.Upgrading) {
                                Log.e("OTA", "OTA PRORESS: ${otaState.appProgress}")

                                BluService.otaProgress.value = otaState.appProgress
                            }

                        }
                    }

                    else -> Log.e("CONNECTION", "model -> ${device?.model}")

                }
            }

            else -> Unit
        }
    }

    companion object {
        private var device: BluArmorDevice? = null

        var otaState: MutableStateFlow<Boolean> = MutableStateFlow(true)
        var otaProgress: MutableStateFlow<Int> = MutableStateFlow(0)
        var musicSharing: MutableStateFlow<Boolean> = MutableStateFlow(true)
        val chitChatState: MutableStateFlow<Boolean> = MutableStateFlow(true)

        var onConnectionStateChangeListener: ((state: ConnectionState) -> Unit)? = null
        fun getDevice(): BluArmorDevice? = BluService.device
    }

    override fun onBind(intent: Intent): IBinder = binder

    private val binder = BluServiceBinder()

    inner class BluServiceBinder : Binder() {
        val service
            get() = this@BluService
    }
}