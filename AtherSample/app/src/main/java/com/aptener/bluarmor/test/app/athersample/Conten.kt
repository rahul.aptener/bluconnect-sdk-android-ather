package com.aptener.bluarmor.test.app.athersample

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun ContentView(vm: MainViewModel) {
    val focusManager = LocalFocusManager.current
    val isFocused = remember { mutableStateOf(false) }

    val connected = vm.connected
    val chitChat = vm.chitChat
    val musicSharing = vm.musicSharing
    val activeChitChatGroupData = vm.activeChitChatGroupData
    val newChitChatGroupData = vm.newChitChatGroupData
    val chitChatGroup = vm.chitChatGroup

    val otaAvailable by BluService.otaState.collectAsState(false)
    val otaProgress by BluService.otaProgress.collectAsState()
    val musicSharingState by BluService.musicSharing.collectAsState()
    val chitChatState by BluService.chitChatState.collectAsState()

    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .padding(top = 50.dp)
            .padding(20.dp)
            .fillMaxSize()
            .clip(RoundedCornerShape(30.dp))
            .background(Color.Black.copy(alpha = 0.06f))
            .padding(20.dp)
    ) {
        Text(
            text = if (connected) "Connected" else "Disconnected",
            color = Color.White,
            fontWeight = FontWeight.SemiBold,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(vertical = 20.dp)
                .fillMaxWidth()
                .align(Alignment.CenterHorizontally)
                .height(70.dp)
                .clip(RoundedCornerShape(20.dp))
                .background(if (connected) Color.Green else Color.Red)
        )


        Row {
            Text("Chit Chat")
            Spacer(modifier = Modifier.weight(1f))
            Switch(
                checked = chitChatState,
                onCheckedChange = { vm.toggleChitChat(it) },
                enabled = connected,
                modifier = Modifier.padding(vertical = 8.dp)
            )
        }

        Row {
            Text("Music Sharing")
            Spacer(modifier = Modifier.weight(1f))
            Switch(
                checked = musicSharingState,
                onCheckedChange = { vm.toggleMusicSharing(it) },
                enabled = connected,
                modifier = Modifier.padding(vertical = 8.dp)
            )
        }

        if (chitChatState) {
            Text(
                text = "Active Group: ${chitChatGroup?.name}",
                color = Color.Black.copy(alpha = 0.7f),
                fontWeight = FontWeight.Medium,
                modifier = Modifier.fillMaxWidth()
            )

            BasicTextField(
                value = activeChitChatGroupData,
                onValueChange = { /*vm.setActiveChitChatGroupData(it) */ },
                textStyle = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Normal,
                    color = Color.Black
                ),
                keyboardOptions = KeyboardOptions.Default.copy(
                    imeAction = ImeAction.Done
                ),
                keyboardActions = KeyboardActions(onDone = {
                    focusManager.clearFocus()
                }),
                modifier = Modifier
                    .fillMaxWidth()
                    .heightIn(min = 50.dp)
                    .background(Color.White, shape = RoundedCornerShape(8.dp))
                    .padding(8.dp)
            )

            Button(
                onClick = {
                    focusManager.clearFocus()
                    vm.createRandomGroup()
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp)
                    .clip(MaterialTheme.shapes.large),
                colors = ButtonDefaults.buttonColors(
                    contentColor = Color.White,
                    containerColor = Color.Black,
                )
            ) {
                Text(
                    text = "New Private Group",
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold,
                    modifier = Modifier
                        .padding(vertical = 10.dp)
                )
            }

            Spacer(modifier = Modifier.height(8.dp))

            BasicTextField(
                value = newChitChatGroupData,
                onValueChange = { vm.updateNewChitChatGroupData(it) },
                textStyle = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Normal,
                    color = Color.Black
                ),
                keyboardOptions = KeyboardOptions.Default.copy(
                    imeAction = ImeAction.Done
                ),
                keyboardActions = KeyboardActions(onDone = {
                    focusManager.clearFocus()
                }),
                modifier = Modifier
                    .fillMaxWidth()
                    .heightIn(min = 50.dp)
                    .background(Color.White, shape = RoundedCornerShape(8.dp))
                    .padding(8.dp)
            )

            Button(
                onClick = {
                    focusManager.clearFocus()
                    vm.joinGroup(newChitChatGroupData)
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp)
                    .clip(MaterialTheme.shapes.large),
                colors = ButtonDefaults.buttonColors(
                    contentColor = Color.White,
                    containerColor = Color.Black,
                )
            ) {
                Text(
                    text = "Join Private Group",
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold,
                    modifier = Modifier
                        .padding(vertical = 10.dp)
                )
            }
        }

        Spacer(modifier = Modifier.height(16.dp))

        if (otaProgress > 0)
            Text("OTA progress: $otaProgress")
        
        Text("Firmware update available: $otaAvailable")

        if (otaAvailable)
            Button(
                onClick = {
                    vm.initOta()
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp)
                    .clip(MaterialTheme.shapes.large),
                colors = ButtonDefaults.buttonColors(
                    contentColor = Color.White,
                    containerColor = Color.Black,
                )
            ) {
                Text(
                    text = "Start OTA",
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold,
                    modifier = Modifier
                        .padding(vertical = 10.dp)
                )
            }
    }
}
