package com.aptener.bluarmor.test.app.athersample

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aptener.bluconnect.client.ConnectionState
import com.aptener.bluconnect.data.RideGridGroup
import com.aptener.bluconnect.data.RideGridGroupInvitation
import com.aptener.bluconnect.device.HALO
import com.aptener.bluconnect.device.control.RideGrid.RideGridState
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    init {
        BluService.onConnectionStateChangeListener = this::onConnectionStateChange
    }

    var connected by mutableStateOf(false)
        private set

    var chitChat by mutableStateOf(true)
        private set

    var musicSharing by mutableStateOf(true)
        private set

    var chitChatState by mutableStateOf(RideGridState.OFF)
        private set

    var chitChatGroup by mutableStateOf<RideGridGroup?>(null)
        private set

    var activeChitChatGroupData by mutableStateOf("")
        private set

    var newChitChatGroupData by mutableStateOf("")
        private set

    private val ridingGroupNames = listOf(
        "Thunder Riders", "Trail Blazers", "Iron Stallions", "Road Warriors", "Silver Serpents",
        "Midnight Riders", "Steel Thunder", "Wild Hogs", "Chrome Crusaders", "Rebel Riders",
        "Viper Vanguards", "Asphalt Avengers", "Highway Outlaws", "Phoenix Riders", "Shadow Stalkers",
        "Urban Nomads", "Ghost Riders", "Desert Wolves", "Night Ravens", "Freewheelers"
    )

    private fun getActiveDevice(): HALO? {
        return BluService.getDevice() as? HALO
    }

    private suspend fun getInvitationData(ridegridGroup: RideGridGroup): String {
        return getActiveDevice()?.rideGridGroupDataSource?.encodeToQrCodeData(ridegridGroup) ?: "ERROR!"
    }

    fun updateActiveChitChatGroupData(data: String) {
        activeChitChatGroupData = data
    }

    fun updateNewChitChatGroupData(data: String) {
        newChitChatGroupData = data
    }

    fun onConnectionStateChange(state: ConnectionState) {
        viewModelScope.launch {
            when (state) {
                is ConnectionState.DeviceReady -> {
                    connected = true
                    updateChitChatGroup(getActiveDevice()?.rideGridGroupDataSource?.getActiveGroup())
                    (state.device as? HALO)?.rideGrid?.observe(this@MainViewModel::rideGridStateObserver)
                }

                else -> {
                    connected = false
                }
            }
        }
    }

    fun rideGridStateObserver(rideGridState: RideGridState) {
        chitChatState = rideGridState
        chitChat = when (rideGridState) {
            RideGridState.DISCOVERY, RideGridState.CONNECTED, RideGridState.ACTIVE_CALL, RideGridState.MUTED -> true
            else -> false
        }
    }

    fun createRandomGroup() {
        viewModelScope.launch {
            chitChatGroup = getActiveDevice()?.rideGridGroupDataSource?.create(ridingGroupNames.random(), isPrimary = true) ?: return@launch
            getActiveDevice()?.rideGrid?.setActiveGridGroupId(chitChatGroup?.groupId ?: return@launch)
            updateChitChatGroup(chitChatGroup)
        }
    }

    fun joinGroup(invitationData: String) {
        viewModelScope.launch {
            val invitation = RideGridGroupInvitation.decodeFromQrCodeData(invitationData) ?: return@launch
            getActiveDevice()?.rideGridGroupDataSource?.join(invitation, true)
            getActiveDevice()?.rideGrid?.setActiveGridGroupId(invitation.ridegridGroup.groupId)
            chitChatGroup = invitation.ridegridGroup
            updateChitChatGroup(chitChatGroup)
        }
    }

    fun toggleChitChat(enabled: Boolean) {
//        chitChat = enabled
        if (enabled) {
            getActiveDevice()?.rideGrid?.turnOn()
        } else {
            getActiveDevice()?.rideGrid?.turnOff()
        }
    }

    fun toggleMusicSharing(enabled: Boolean) {
//        musicSharing = enabled
        getActiveDevice()?.rideGrid?.toggleMusicSharing(enable = enabled)
    }

    fun updateChitChatGroup(newGroup: RideGridGroup?) {
        viewModelScope.launch {
            chitChatGroup = newGroup
            if (newGroup?.privateGroup == true) {
                activeChitChatGroupData = getInvitationData(newGroup)
            }
        }
    }

    fun initOta() {
        getActiveDevice()?.otaSwitch?.switchToOtaMode()
    }

}
